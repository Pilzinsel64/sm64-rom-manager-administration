﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM64RomManager.LangRes_Collector
{
    internal class LangResFilterConfig
    {
        public string[] FileNameBlackList { get; set; }
        public string[] FileNameWhiteList { get; set; }
        public string[] PropNameBlackList { get; set; }
    }
}
