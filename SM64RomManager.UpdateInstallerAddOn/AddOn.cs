﻿using System.IO;
using Pilz.Updating.UpdateInstaller.Lib;
using Pilz.Plugins;

namespace UpdateInstaller
{
    public class AddOn : IPlugin
    {
        public string Name => "SM64 ROM Manager UpdateInstaller AddOn";

        public AddOn(Pilz.Updating.UpdateInstaller.Lib.UpdateInstaller installer)
        {
            installer.OnStep += Installer_OnStep;
        }

        private void Installer_OnStep(object sender, UpdateInstallerStepEventArgs e)
        {
            if (e.Step == UpdateInstallerStep.CopyFiles && e.State == UpdateInstallerStepState.PreEvent
                /*&& config.NewApplicationVersion is null && config.CurrentApplicationVersion is null*/)
            {
                Directory.Delete(Path.Combine(e.UpdateInstaller.Configuration.HostApplicationPath, @"Data\Importer Presets"), true);

                Directory.Delete(Path.Combine(e.UpdateInstaller.Configuration.HostApplicationPath, @"Data\Tweaks\Reviewed"), true);

                var pCustomObjectDatabase = Path.Combine(e.UpdateInstaller.Configuration.HostApplicationPath, @"Data\Custom Objects\Reviewed");
                if (Directory.Exists(pCustomObjectDatabase))
                    Directory.Delete(pCustomObjectDatabase, true);
            }
        }
    }
}