namespace SM64RomManager.ProgressUpdater
{
    partial class DiscordSettingsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiscordSettingsDialog));
            this.panel1 = new System.Windows.Forms.Panel();
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.dotsRingWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement();
            this.RadButton_LoadServersAndChannels = new Telerik.WinControls.UI.RadButton();
            this.RadTextBoxControl_UrlExtraCounterParamName = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBoxControl_ImageBaseURL = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBoxControl_DiscordBotToken = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadCheckBox_EnableDiscordUpload = new Telerik.WinControls.UI.RadCheckBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_LoadServersAndChannels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBoxControl_UrlExtraCounterParamName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBoxControl_ImageBaseURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBoxControl_DiscordBotToken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_EnableDiscordUpload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.radWaitingBar1);
            this.panel1.Controls.Add(this.radTreeView1);
            this.panel1.Controls.Add(this.RadButton_LoadServersAndChannels);
            this.panel1.Controls.Add(this.RadTextBoxControl_UrlExtraCounterParamName);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.RadTextBoxControl_ImageBaseURL);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.RadTextBoxControl_DiscordBotToken);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadCheckBox_EnableDiscordUpload);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(511, 281);
            this.panel1.TabIndex = 0;
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.AssociatedControl = this.radTreeView1;
            this.radWaitingBar1.Location = new System.Drawing.Point(127, 241);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(70, 70);
            this.radWaitingBar1.TabIndex = 9;
            this.radWaitingBar1.Text = "radWaitingBar1";
            this.radWaitingBar1.WaitingIndicators.Add(this.dotsRingWaitingBarIndicatorElement1);
            this.radWaitingBar1.WaitingIndicatorSize = new System.Drawing.Size(100, 14);
            this.radWaitingBar1.WaitingSpeed = 50;
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsRing;
            this.radWaitingBar1.WaitingStarted += new System.EventHandler(this.RadWaitingBar1_WaitingStarted);
            this.radWaitingBar1.WaitingStopped += new System.EventHandler(this.RadWaitingBar1_WaitingStopped);
            // 
            // radTreeView1
            // 
            this.radTreeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTreeView1.ItemHeight = 28;
            this.radTreeView1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView1.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView1.Location = new System.Drawing.Point(289, 34);
            this.radTreeView1.Name = "radTreeView1";
            this.radTreeView1.Size = new System.Drawing.Size(219, 244);
            this.radTreeView1.TabIndex = 8;
            this.radTreeView1.SelectedNodeChanged += new Telerik.WinControls.UI.RadTreeView.RadTreeViewEventHandler(this.RadTreeView1_SelectedNodeChanged);
            // 
            // dotsRingWaitingBarIndicatorElement1
            // 
            this.dotsRingWaitingBarIndicatorElement1.Name = "dotsRingWaitingBarIndicatorElement1";
            // 
            // RadButton_LoadServersAndChannels
            // 
            this.RadButton_LoadServersAndChannels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_LoadServersAndChannels.Location = new System.Drawing.Point(289, 4);
            this.RadButton_LoadServersAndChannels.Name = "RadButton_LoadServersAndChannels";
            this.RadButton_LoadServersAndChannels.Size = new System.Drawing.Size(219, 24);
            this.RadButton_LoadServersAndChannels.TabIndex = 7;
            this.RadButton_LoadServersAndChannels.Text = "Load Server and Channels";
            this.RadButton_LoadServersAndChannels.Click += new System.EventHandler(this.ButtonX1_Click);
            // 
            // RadTextBoxControl_UrlExtraCounterParamName
            // 
            this.RadTextBoxControl_UrlExtraCounterParamName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadTextBoxControl_UrlExtraCounterParamName.Location = new System.Drawing.Point(3, 155);
            this.RadTextBoxControl_UrlExtraCounterParamName.Name = "RadTextBoxControl_UrlExtraCounterParamName";
            this.RadTextBoxControl_UrlExtraCounterParamName.Size = new System.Drawing.Size(280, 22);
            this.RadTextBoxControl_UrlExtraCounterParamName.TabIndex = 6;
            this.RadTextBoxControl_UrlExtraCounterParamName.Click += new System.EventHandler(this.TextBoxX_UrlExtraCounterParam_TextChanged);
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(3, 131);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(164, 18);
            this.radLabel3.TabIndex = 5;
            this.radLabel3.Text = "URL extra counter param name:";
            // 
            // RadTextBoxControl_ImageBaseURL
            // 
            this.RadTextBoxControl_ImageBaseURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadTextBoxControl_ImageBaseURL.Location = new System.Drawing.Point(3, 103);
            this.RadTextBoxControl_ImageBaseURL.Name = "RadTextBoxControl_ImageBaseURL";
            this.RadTextBoxControl_ImageBaseURL.Size = new System.Drawing.Size(280, 22);
            this.RadTextBoxControl_ImageBaseURL.TabIndex = 4;
            this.RadTextBoxControl_ImageBaseURL.Click += new System.EventHandler(this.TextBoxX_ImgBaseURL_TextChanged);
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(3, 79);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(89, 18);
            this.radLabel2.TabIndex = 3;
            this.radLabel2.Text = "Image Base URL:";
            // 
            // RadTextBoxControl_DiscordBotToken
            // 
            this.RadTextBoxControl_DiscordBotToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadTextBoxControl_DiscordBotToken.Location = new System.Drawing.Point(3, 51);
            this.RadTextBoxControl_DiscordBotToken.Name = "RadTextBoxControl_DiscordBotToken";
            this.RadTextBoxControl_DiscordBotToken.Size = new System.Drawing.Size(280, 22);
            this.RadTextBoxControl_DiscordBotToken.TabIndex = 2;
            this.RadTextBoxControl_DiscordBotToken.Click += new System.EventHandler(this.TextBoxX_BotToken_TextChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 27);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(100, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Discord Bot Token:";
            // 
            // RadCheckBox_EnableDiscordUpload
            // 
            this.RadCheckBox_EnableDiscordUpload.Location = new System.Drawing.Point(3, 3);
            this.RadCheckBox_EnableDiscordUpload.Name = "RadCheckBox_EnableDiscordUpload";
            this.RadCheckBox_EnableDiscordUpload.Size = new System.Drawing.Size(137, 18);
            this.RadCheckBox_EnableDiscordUpload.TabIndex = 0;
            this.RadCheckBox_EnableDiscordUpload.Text = "Enable Discord Upload";
            this.RadCheckBox_EnableDiscordUpload.Click += new System.EventHandler(this.CheckBoxX_EnableDiscordUpload_CheckedChanged);
            // 
            // DiscordSettingsDialog
            // 
            this.ClientSize = new System.Drawing.Size(511, 281);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DiscordSettingsDialog";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Discord Settings";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_LoadServersAndChannels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBoxControl_UrlExtraCounterParamName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBoxControl_ImageBaseURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBoxControl_DiscordBotToken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_EnableDiscordUpload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        private Telerik.WinControls.UI.RadButton RadButton_LoadServersAndChannels;
        private Telerik.WinControls.UI.RadTextBoxControl RadTextBoxControl_UrlExtraCounterParamName;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBoxControl RadTextBoxControl_ImageBaseURL;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBoxControl RadTextBoxControl_DiscordBotToken;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_EnableDiscordUpload;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement dotsRingWaitingBarIndicatorElement1;
    }
}