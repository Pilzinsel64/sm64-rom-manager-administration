﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace SM64RomManager.ProgressUpdater
{
    public partial class WebDavSettingsDialog : RadForm
    {
        private readonly Settings settings;
        private readonly Dictionary<string, string> propNameLabel = new Dictionary<string, string>();
        private readonly List<string> propNameChar = new List<string>();

        public WebDavSettingsDialog(Settings settings)
        {
            this.settings = settings;
            InitializeComponent();

            SetupPropDics();
            radDataLayout1.ItemInitializing += RadDataLayout1_ItemInitializing;
            radDataLayout1.DataSource = settings;

            #region "Example-Methods for RadPropertyGrid"

            //radPropertyGrid1.CreateItemElement += RadPropertyGrid1_CreateItemElement;
            //radPropertyGrid1.EditorInitialized += RadPropertyGrid1_EditorInitialized;
            //radPropertyGrid1.PropertyGridElement.PropertyTableElement.ListSource.CollectionChanged += ListSource_CollectionChanged;
            //radPropertyGrid1.SelectedObjectChanging += PropertyTableElement_SelectedObjectChanging;
            //radPropertyGrid1.SelectedObjectChanged += PropertyTableElement_SelectedObjectChanged;
            //radPropertyGrid1.SelectedObject = settings;

            #endregion
        }

        #region "Example-Methods for RadPropertyGrid"

        //private void PropertyTableElement_SelectedObjectChanging(object sender, PropertyGridSelectedObjectChangingEventArgs e)
        //{
        //}

        //private void ListSource_CollectionChanged(object sender, Telerik.WinControls.Data.NotifyCollectionChangedEventArgs e)
        //{
        //}

        //private void PropertyTableElement_SelectedObjectChanged(object sender, PropertyGridSelectedObjectChangedEventArgs e)
        //{
        //}

        //private void RadPropertyGrid1_EditorInitialized(object sender, PropertyGridItemEditorInitializedEventArgs e)
        //{
        //}

        //private void RadPropertyGrid1_CreateItemElement(object sender, CreatePropertyGridItemElementEventArgs e)
        //{
        //    if (propNameLabel.ContainsKey(e.Item.Name))
        //        e.Item.Label = propNameLabel[e.Item.Name];
        //    else
        //    {
        //        ((PropertyGridItem)e.Item).Visible = false;
        //        //((PropertyGridItem)e.Item).Enabled = false;
        //    }
        //}

        #endregion

        private void SetupPropDics()
        {
            propNameLabel.Clear();
            propNameLabel.Add(nameof(Settings.WebDavUri), "WebDav URL:");
            propNameLabel.Add(nameof(Settings.WebDavUsr), "WebDav User:");
            propNameLabel.Add(nameof(Settings.WebDavPwd), "WebDav Password:");
            propNameLabel.Add(nameof(Settings.ProxyUsr), "Proxy User:");
            propNameLabel.Add(nameof(Settings.ProxyPwd), "Proxy Password:");

            propNameChar.Clear();
            propNameChar.Add(nameof(Settings.WebDavPwd));
            propNameChar.Add(nameof(Settings.ProxyPwd));
        }

        private void RadDataLayout1_EditorInitializing(object sender, EditorInitializingEventArgs e)
        {
            if (!propNameLabel.ContainsKey(e.Property.Name))
                e.Cancel = true;
            else if (propNameChar.Contains(e.Property.Name) && e.EditorType == typeof(RadTextBox))
                ((RadTextBox)e.Editor).UseSystemPasswordChar = true;
        }

        private void RadDataLayout1_ItemInitializing(object sender, DataLayoutItemInitializingEventArgs e)
        {
            if (propNameLabel.ContainsKey(e.Item.AccessibleName))
                e.Item.Text = propNameLabel[e.Item.AccessibleName];
        }
    }
}
