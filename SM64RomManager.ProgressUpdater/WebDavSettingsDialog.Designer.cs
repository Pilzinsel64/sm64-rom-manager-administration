﻿namespace SM64RomManager.ProgressUpdater
{
    partial class WebDavSettingsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WebDavSettingsDialog));
            this.radDataLayout1 = new Telerik.WinControls.UI.RadDataLayout();
            this.radPropertyGrid1 = new Telerik.WinControls.UI.RadPropertyGrid();
            ((System.ComponentModel.ISupportInitialize)(this.radDataLayout1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDataLayout1.LayoutControl)).BeginInit();
            this.radDataLayout1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPropertyGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radDataLayout1
            // 
            this.radDataLayout1.BackColor = System.Drawing.Color.Transparent;
            this.radDataLayout1.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // radDataLayout1.LayoutControl
            // 
            this.radDataLayout1.LayoutControl.BackColor = System.Drawing.Color.Transparent;
            this.radDataLayout1.LayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDataLayout1.LayoutControl.DrawBorder = false;
            this.radDataLayout1.LayoutControl.Location = new System.Drawing.Point(0, 0);
            this.radDataLayout1.LayoutControl.Name = "LayoutControl";
            this.radDataLayout1.LayoutControl.Size = new System.Drawing.Size(584, 132);
            this.radDataLayout1.LayoutControl.TabIndex = 0;
            this.radDataLayout1.Location = new System.Drawing.Point(0, 0);
            this.radDataLayout1.Name = "radDataLayout1";
            this.radDataLayout1.Size = new System.Drawing.Size(584, 132);
            this.radDataLayout1.TabIndex = 0;
            this.radDataLayout1.EditorInitializing += new Telerik.WinControls.UI.EditorInitializingEventHandler(this.RadDataLayout1_EditorInitializing);
            // 
            // radPropertyGrid1
            // 
            this.radPropertyGrid1.ItemHeight = 28;
            this.radPropertyGrid1.ItemIndent = 28;
            this.radPropertyGrid1.Location = new System.Drawing.Point(292, 0);
            this.radPropertyGrid1.Name = "radPropertyGrid1";
            this.radPropertyGrid1.Size = new System.Drawing.Size(280, 300);
            this.radPropertyGrid1.TabIndex = 1;
            this.radPropertyGrid1.Visible = false;
            // 
            // WebDavSettingsDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 15);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 132);
            this.Controls.Add(this.radDataLayout1);
            this.Controls.Add(this.radPropertyGrid1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WebDavSettingsDialog";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "WebDav Settings";
            ((System.ComponentModel.ISupportInitialize)(this.radDataLayout1.LayoutControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDataLayout1)).EndInit();
            this.radDataLayout1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPropertyGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadDataLayout radDataLayout1;
        private Telerik.WinControls.UI.RadPropertyGrid radPropertyGrid1;
    }
}