using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace SM64RomManager.ProgressUpdater
{
    public partial class DiscordSettingsDialog : RadForm
    {
        private readonly Settings settings;

        public DiscordSettingsDialog(Settings settings)
        {
            InitializeComponent();
            this.settings = settings;
            RadTextBoxControl_DiscordBotToken.Text = settings.DiscordBotToken;
            RadTextBoxControl_ImageBaseURL.Text = settings.DiscordMsgBaseURL;
            RadTextBoxControl_UrlExtraCounterParamName.Text = settings.DiscordMsgParamCounter;
            RadCheckBox_EnableDiscordUpload.Checked = settings.DiscordUploadEnabled;
        }

        private void TextBoxX_BotToken_TextChanged(object sender, EventArgs e)
        {
            settings.DiscordBotToken = RadTextBoxControl_DiscordBotToken.Text.Trim();
        }

        private void TextBoxX_ImgBaseURL_TextChanged(object sender, EventArgs e)
        {
            settings.DiscordMsgBaseURL = RadTextBoxControl_ImageBaseURL.Text.Trim();
        }

        private void TextBoxX_UrlExtraCounterParam_TextChanged(object sender, EventArgs e)
        {
            settings.DiscordMsgParamCounter = RadTextBoxControl_UrlExtraCounterParamName.Text.Trim();
        }

        private void ButtonX1_Click(object sender, EventArgs e)
        {
            radWaitingBar1.StartWaiting();
            RadButton_LoadServersAndChannels.Enabled = false;

            var dmgr = new DiscordMgr(settings);
            bool hasError = false;
            dmgr.LoggedMsg += (ssender, msg, isError) => { if (hasError) hasError = true; };
            dmgr.Start();

            while (!dmgr.IsReady && !hasError)
            {
                Application.DoEvents();
            }

            if (hasError)
            {
                RadMessageBox.Show("Entwender deaktiviert oder Token ist falsch.", string.Empty, MessageBoxButtons.OK, RadMessageIcon.Error);
            }
            else
            {
                radTreeView1.BeginUpdate();
                radTreeView1.Nodes.Clear();

                foreach (var guild in dmgr.Client.Guilds)
                {
                    var nGuild = new RadTreeNode()
                    {
                        Name = "g" + guild.Id,
                        Text = guild.Name,
                        Tag = guild.Id,
                        Expanded = true
                    };

                    foreach (var channel in guild.TextChannels)
                    {
                        var nChannel = new RadTreeNode()
                        {
                            Name = "c" + channel.Id,
                            Text = "#" + channel.Name,
                            Tag = channel.Id
                        };
                        
                        nGuild.Nodes.Add(nChannel);
                    }

                    radTreeView1.Nodes.Add(nGuild);
                }

                radTreeView1.EndUpdate();
                radTreeView1.Refresh();
                dmgr.Stop();
            }

            radWaitingBar1.StopWaiting();
        }

        private void RadTreeView1_SelectedNodeChanged(object sender, RadTreeViewEventArgs e)
        {
            if (e.Node.Name.StartsWith("c"))
            {
                settings.DiscordChannelID = (ulong)e.Node.Tag;
                settings.DiscordGuildID = (ulong)e.Node.Parent.Tag;
            }
        }

        private void CheckBoxX_EnableDiscordUpload_CheckedChanged(object sender, EventArgs e)
        {
            settings.DiscordUploadEnabled = RadCheckBox_EnableDiscordUpload.Checked;
        }

        private void RadWaitingBar1_WaitingStarted(object sender, EventArgs e)
        {
            RadButton_LoadServersAndChannels.Enabled = false;
        }

        private void RadWaitingBar1_WaitingStopped(object sender, EventArgs e)
        {
            RadButton_LoadServersAndChannels.Enabled = true;
        }
    }
}