﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace SM64RomManager.ProgressUpdater
{
    [DesignerGenerated()]
    public partial class Form1 : Telerik.WinControls.UI.RadForm
    {

        // Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Wird vom Windows Form-Designer benötigt.
        private System.ComponentModel.IContainer components = new System.ComponentModel.Container();

        // Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
        // Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
        // Das Bearbeiten mit dem Code-Editor ist nicht möglich.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Panel1 = new System.Windows.Forms.Panel();
            this.RadButton_InvertImage = new Telerik.WinControls.UI.RadButton();
            this.RadButton_PasteFromDocument = new Telerik.WinControls.UI.RadButton();
            this.RadButton_PasteFromClipboard = new Telerik.WinControls.UI.RadButton();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.RadButton_Upload = new Telerik.WinControls.UI.RadButton();
            this.RadDropDownList_Version = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_DiscordSetup = new Telerik.WinControls.UI.RadButton();
            this.RadButton_SetupWebDav = new Telerik.WinControls.UI.RadButton();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsRingWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_InvertImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_PasteFromDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_PasteFromClipboard)).BeginInit();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Upload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Version)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_DiscordSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_SetupWebDav)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.Panel1.Controls.Add(this.RadButton_InvertImage);
            this.Panel1.Controls.Add(this.RadButton_PasteFromDocument);
            this.Panel1.Controls.Add(this.RadButton_PasteFromClipboard);
            this.Panel1.Controls.Add(this.Panel2);
            this.Panel1.Controls.Add(this.PictureBox1);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel1.Location = new System.Drawing.Point(0, 0);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(694, 508);
            this.Panel1.TabIndex = 0;
            // 
            // RadButton_InvertImage
            // 
            this.RadButton_InvertImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_InvertImage.Location = new System.Drawing.Point(563, 33);
            this.RadButton_InvertImage.Name = "RadButton_InvertImage";
            this.RadButton_InvertImage.Size = new System.Drawing.Size(128, 24);
            this.RadButton_InvertImage.TabIndex = 22;
            this.RadButton_InvertImage.Text = "Invert Image";
            this.RadButton_InvertImage.Click += new System.EventHandler(this.buttonX1_Click_1);
            // 
            // RadButton_PasteFromDocument
            // 
            this.RadButton_PasteFromDocument.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_PasteFromDocument.Location = new System.Drawing.Point(429, 3);
            this.RadButton_PasteFromDocument.Name = "RadButton_PasteFromDocument";
            this.RadButton_PasteFromDocument.Size = new System.Drawing.Size(128, 24);
            this.RadButton_PasteFromDocument.TabIndex = 1;
            this.RadButton_PasteFromDocument.Text = "Paste from Document";
            this.RadButton_PasteFromDocument.Click += new System.EventHandler(this.ButtonX_PasteDocument_Click);
            // 
            // RadButton_PasteFromClipboard
            // 
            this.RadButton_PasteFromClipboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_PasteFromClipboard.Location = new System.Drawing.Point(563, 3);
            this.RadButton_PasteFromClipboard.Name = "RadButton_PasteFromClipboard";
            this.RadButton_PasteFromClipboard.Size = new System.Drawing.Size(128, 24);
            this.RadButton_PasteFromClipboard.TabIndex = 21;
            this.RadButton_PasteFromClipboard.Text = "Paste from Clipboard";
            this.RadButton_PasteFromClipboard.Click += new System.EventHandler(this.ButtonX1_Click);
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.RadButton_Upload);
            this.Panel2.Controls.Add(this.RadDropDownList_Version);
            this.Panel2.Controls.Add(this.radLabel1);
            this.Panel2.Controls.Add(this.RadButton_DiscordSetup);
            this.Panel2.Controls.Add(this.RadButton_SetupWebDav);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel2.Location = new System.Drawing.Point(0, 0);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(200, 508);
            this.Panel2.TabIndex = 1;
            // 
            // RadButton_Upload
            // 
            this.RadButton_Upload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RadButton_Upload.Location = new System.Drawing.Point(3, 457);
            this.RadButton_Upload.Name = "RadButton_Upload";
            this.RadButton_Upload.Size = new System.Drawing.Size(194, 48);
            this.RadButton_Upload.TabIndex = 18;
            this.RadButton_Upload.Text = "Upload";
            this.RadButton_Upload.Click += new System.EventHandler(this.ButtonX_Upload_Click);
            // 
            // RadDropDownList_Version
            // 
            this.RadDropDownList_Version.DropDownAnimationEnabled = true;
            this.RadDropDownList_Version.Location = new System.Drawing.Point(3, 87);
            this.RadDropDownList_Version.Name = "RadDropDownList_Version";
            this.RadDropDownList_Version.NullText = "e.g. 1.2.0.0";
            this.RadDropDownList_Version.Size = new System.Drawing.Size(194, 24);
            this.RadDropDownList_Version.TabIndex = 19;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 63);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(46, 18);
            this.radLabel1.TabIndex = 18;
            this.radLabel1.Text = "Version:";
            // 
            // RadButton_DiscordSetup
            // 
            this.RadButton_DiscordSetup.Location = new System.Drawing.Point(3, 33);
            this.RadButton_DiscordSetup.Name = "RadButton_DiscordSetup";
            this.RadButton_DiscordSetup.Size = new System.Drawing.Size(194, 24);
            this.RadButton_DiscordSetup.TabIndex = 17;
            this.RadButton_DiscordSetup.Text = "Setup Discord Bot";
            this.RadButton_DiscordSetup.Click += new System.EventHandler(this.ButtonX_DiscordSetup_Click);
            // 
            // RadButton_SetupWebDav
            // 
            this.RadButton_SetupWebDav.Location = new System.Drawing.Point(3, 3);
            this.RadButton_SetupWebDav.Name = "RadButton_SetupWebDav";
            this.RadButton_SetupWebDav.Size = new System.Drawing.Size(194, 24);
            this.RadButton_SetupWebDav.TabIndex = 16;
            this.RadButton_SetupWebDav.Text = "Setup WebDav Client";
            this.RadButton_SetupWebDav.Click += new System.EventHandler(this.ButtonX_SetupWebDav_Click);
            // 
            // PictureBox1
            // 
            this.PictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox1.Location = new System.Drawing.Point(0, 0);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(694, 508);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox1.TabIndex = 0;
            this.PictureBox1.TabStop = false;
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.AssociatedControl = this.RadButton_Upload;
            this.radWaitingBar1.Location = new System.Drawing.Point(29, -23);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(70, 70);
            this.radWaitingBar1.TabIndex = 21;
            this.radWaitingBar1.WaitingIndicators.Add(this.dotsRingWaitingBarIndicatorElement1);
            this.radWaitingBar1.WaitingIndicatorSize = new System.Drawing.Size(100, 14);
            this.radWaitingBar1.WaitingSpeed = 50;
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsRing;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(radWaitingBar1.GetChildAt(0))).WaitingIndicatorSize = new System.Drawing.Size(100, 14);
            ((Telerik.WinControls.UI.RadWaitingBarElement)(radWaitingBar1.GetChildAt(0))).WaitingSpeed = 50;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(radWaitingBar1.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsRing;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(radWaitingBar1.GetChildAt(0).GetChildAt(0).GetChildAt(0))).ProgressOrientation = Telerik.WinControls.ProgressOrientation.Right;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(radWaitingBar1.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // dotsRingWaitingBarIndicatorElement1
            // 
            this.dotsRingWaitingBarIndicatorElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            this.dotsRingWaitingBarIndicatorElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentBounds;
            this.dotsRingWaitingBarIndicatorElement1.Name = "dotsRingWaitingBarIndicatorElement1";
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 15);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 508);
            this.Controls.Add(this.Panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SM64RM Progress Updater";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_InvertImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_PasteFromDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_PasteFromClipboard)).EndInit();
            this.Panel2.ResumeLayout(false);
            this.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Upload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Version)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_DiscordSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_SetupWebDav)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        private Panel Panel1;



        private PictureBox PictureBox1;


        private Panel Panel2;










        private Telerik.WinControls.UI.RadButton RadButton_SetupWebDav;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Version;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton RadButton_DiscordSetup;
        private Telerik.WinControls.UI.RadButton RadButton_Upload;
        private Telerik.WinControls.UI.RadButton RadButton_InvertImage;
        private Telerik.WinControls.UI.RadButton RadButton_PasteFromDocument;
        private Telerik.WinControls.UI.RadButton RadButton_PasteFromClipboard;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement dotsRingWaitingBarIndicatorElement1;
    }
}