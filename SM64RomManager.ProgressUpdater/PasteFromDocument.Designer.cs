﻿namespace SM64RomManager.ProgressUpdater
{
    partial class PasteFromDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PasteFromDocument));
            this.RadButton_Paste = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radTextBoxControl1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadListControl_Paragraph = new Telerik.WinControls.UI.RadListControl();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Paste)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadListControl_Paragraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadButton_Paste
            // 
            this.RadButton_Paste.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_Paste.Location = new System.Drawing.Point(205, 286);
            this.RadButton_Paste.Name = "RadButton_Paste";
            this.RadButton_Paste.Size = new System.Drawing.Size(110, 24);
            this.RadButton_Paste.TabIndex = 0;
            this.RadButton_Paste.Text = "Paste";
            this.RadButton_Paste.Click += new System.EventHandler(this.ButtonX_Paste_Click);
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.RadButton_Cancel.Location = new System.Drawing.Point(321, 286);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(110, 24);
            this.RadButton_Cancel.TabIndex = 1;
            this.RadButton_Cancel.Text = "Cancel";
            // 
            // radTextBoxControl1
            // 
            this.radTextBoxControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBoxControl1.Location = new System.Drawing.Point(3, 3);
            this.radTextBoxControl1.Name = "radTextBoxControl1";
            this.radTextBoxControl1.NullText = "Document URL";
            this.radTextBoxControl1.Size = new System.Drawing.Size(428, 22);
            this.radTextBoxControl1.TabIndex = 2;
            this.radTextBoxControl1.TextChanged += new System.EventHandler(this.textBoxX1_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.RadListControl_Paragraph);
            this.panel1.Controls.Add(this.RadButton_Cancel);
            this.panel1.Controls.Add(this.radTextBoxControl1);
            this.panel1.Controls.Add(this.RadButton_Paste);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(434, 313);
            this.panel1.TabIndex = 4;
            // 
            // RadListControl_Paragraph
            // 
            this.RadListControl_Paragraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadListControl_Paragraph.ItemHeight = 24;
            this.RadListControl_Paragraph.Location = new System.Drawing.Point(3, 31);
            this.RadListControl_Paragraph.Name = "RadListControl_Paragraph";
            this.RadListControl_Paragraph.Size = new System.Drawing.Size(428, 249);
            this.RadListControl_Paragraph.TabIndex = 4;
            // 
            // PasteFromDocument
            // 
            this.AcceptButton = this.RadButton_Paste;
            this.AutoScaleBaseSize = new System.Drawing.Size(7, 15);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.RadButton_Cancel;
            this.ClientSize = new System.Drawing.Size(434, 313);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PasteFromDocument";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Paste From Document";
            this.Load += new System.EventHandler(this.PasteFromDocument_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Paste)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadListControl_Paragraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton RadButton_Paste;
        private Telerik.WinControls.UI.RadButton RadButton_Cancel;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadListControl RadListControl_Paragraph;
    }
}